#!/usr/bin/env python
"""
Stand alone httpd server to run WebService for accessing the NECOFS Forecast models at:
  http://www.smast.umassd.edu:8080/thredds/forecasts.html
"""
import sys
import argparse
import BaseHTTPServer
import CGIHTTPServer
import cgitb; cgitb.enable()  ## This line enables CGI error reporting

parser = argparse.ArgumentParser()
parser.add_argument("PORT", help="PORT to run http on localhost. Default 8300", nargs='?', default='8300')
parser.add_argument("log_file", help="Name of the http log file. Default tds_ws_httpd.log", nargs='*', default='tds_ws_httpd.log')
args = parser.parse_args()

PORT = int(args.PORT)

print "Port:", PORT
print "HTTPD log:", args.log_file

sys.stderr = open(args.log_file, 'w', 0)

try:
  server = BaseHTTPServer.HTTPServer
  handler = CGIHTTPServer.CGIHTTPRequestHandler
  server_address = ("", PORT)
  handler.cgi_directories = ["/cgi-bin"]
  print "serving at port", PORT
  print "Ctl-C to quit"
  httpd = server(server_address, handler)
  httpd.serve_forever()
except KeyboardInterrupt:
  print 'Shutting down the web server on port', PORT
  httpd.socket.close()

