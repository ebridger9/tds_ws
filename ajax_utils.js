//  requires jquery 1.7

//  usage: loadAJAX(json_url, myJSONCallback);

function loadAJAX(json_url, success_function)
{
    if(!json_url){
        alert("loadAJAX ERROR. json_url required.");
        return null;
    }

    if(!success_function){
        success_function = defaultJSONCallback;
    }

    $.ajax({
        type: "GET",
        url:  json_url,
        dataType: "json",  // returns an JSON DOM object
        success: success_function,
        error: function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ' ' + errorThrown);
        }
    });
}

function defaultJSONCallback(jsonData)
{
  if(jsonData)
  {
    alert("OK");
  }
    
}

