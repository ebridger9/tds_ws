#! /usr/bin/env python
import os
import argparse
import json
import netCDF4
import numpy as np
from utils.nc4_geo_idx import near_ugrid_idx
from utils.roundDateTime import roundTime
"""
Note: FVCOM NECOFS and all unstructured grids work on the basis of a single grid id based on lat/lon. 
  see: utils.nc4_geo_idx.near_ugrid_idx()
"""
def getNECOFS(tds_url, lat, lon, tf, var_name):
    """
    getNECOFS()  get the time series forecast from the NECOFS FVCOM model
      tds_url  -THREDDS OPeNDAP file url
      lat      -latitude.  decimal degrees
      lon      -longitude. decimal degrees
      tf       -time format iso or hc - return times as iso formatted strings or milliseconds since the epoch, suitable for HighCharts
      var_name -  netCDF variable name, e.g. hs (wave height) or zeta (water level)
    """
    try:
      nci = netCDF4.Dataset(tds_url)
    except RuntimeError:
      #print "ERROR: Could not open " + tds_url
      json_error = {
          "ERROR" : "Could not open " + tds_url
      }
      return json.dumps(json_error, indent=4)
    
    times = nci.variables['time']
    if not len(times):
      #print "ERROR: No times in " + tds_url
      json_error = {
          "ERROR" : "No times in " + tds_url
      }
      return json.dumps(json_error, indent=4)
    
    times = netCDF4.num2date(times[:],times.units)
    lats = nci.variables['lat'][:]
    lons = nci.variables['lon'][:]
    grid_idx = near_ugrid_idx( lats, lat, lons, lon)
    tds_var =  nci.variables[var_name]
    tds_var_attr =  tds_var.__dict__

    # round the times as strings this is needed. netcdf num2date for float julian days gives rounding errors in minutes.
    rnd_times = []
    for jd in times:
      rnd_times.append( roundTime(jd,roundTo=60*60))

    assert( len(times) == len(rnd_times) == len(tds_var) )

    min = tds_var[:, grid_idx].min()
    max = tds_var[:, grid_idx].max()
    # Convert times to iso strings. This is for start and end time metada
    iso_times = [ t.isoformat() for t in rnd_times]

    if tf == 'hc':
      # netcdf4 sse_units string
      sse_units = "seconds since 1970-01-01 00:00:00"
      # Convert to seconds, then milliseconds
      sse_jd = netCDF4.date2num(rnd_times, sse_units).astype('int64')
      msse = sse_jd * 1000
      # create a np.recarray() which allows us to combine ints (msse) and floats sst. Something like pandas.
      ra = np.recarray( len(msse), dtype=[ ('msse', 'int64'), (var_name, tds_var.dtype)])
      ra['msse'] = msse
      ra[var_name] = tds_var[:, grid_idx]

    if tf == 'iso':
      # create a np.recarray() which allows us to combine string (iso_times) and floats sst. Something like pandas.
      ra = np.recarray( len(iso_times), dtype=[ ('iso_times', 'S20'), (var_name, tds_var.dtype)])
      ra['iso_times'] = iso_times
      ra[var_name] = tds_var[:, grid_idx]

    nci.close()

    standard_name = ''
    if 'standard_name' in tds_var_attr:
      standard_name = tds_var_attr['standard_name']
    elif 'long_name' in tds_var_attr:
      standard_name = tds_var_attr['long_name']

    json_data = {
      "model_name": os.path.splitext(os.path.basename(tds_url))[0],
      "source_url": tds_url,
      "lat": lat,
      "lon": lon,
      "var_name": var_name,
      "standard_name": standard_name,
      "units": tds_var_attr['units'],
      "grid_idx": grid_idx,
      "start_time": iso_times[0],
      "end_time": iso_times[-1],
# not serializable error
#      "min_value": min,
#      "max_value": max,
      "data": ra.tolist()
    }

    return json.dumps(json_data, indent=4)

################################################################
def run():
    parser = argparse.ArgumentParser()
    parser.add_argument("-lat", "--latitude", help="Latitude. Default 43.18", nargs='?', default=43.18)
    parser.add_argument("-lon", "--longitude", help="Longitude. Default -70.42", nargs='?', default=-70.42)
    parser.add_argument("-nt", "--necofs_type", help="NECOFS model type. water_level | wave. Default: water_level.", nargs='?', default="water_level")
    parser.add_argument("-d", "--domain", help="NECOFS model domain region. GOM3 | MASSBAY. Default: GOM3", nargs='?', default="GOM3")
    parser.add_argument("-tf", "--time_format", help="JSON time formats. iso strings or Highcharts (mmsse). iso | hc. Default: iso", nargs='?', default="iso")
    args = parser.parse_args()
    
    lat = float(args.latitude)
    lon = float(args.longitude)

    if args.time_format not in ['iso', 'hc']:
      print "Unknown time format", args.time_format
      exit()

    if args.domain not in ['GOM3', 'MASSBAY']:
      print "Unknown model domain", args.domain
      exit()
    if args.necofs_type not in ['water_level', 'wave']:
      print "Unknown model type", args.necofs_type
      exit()
    
    # Should handle wave forecast as well.
    # NECOFS GOM3 is default
    tds_url='http://www.smast.umassd.edu:8080/thredds/dodsC/FVCOM/NECOFS/Forecasts/NECOFS_GOM3_FORECAST.nc'
    # wave model only has GOM3 domain
    if args.necofs_type == 'wave':
      tds_url = 'http://www.smast.umassd.edu:8080/thredds/dodsC/FVCOM/NECOFS/Forecasts/NECOFS_WAVE_FORECAST.nc'
      args.domain = 'GOM3'
    
    
    if args.domain == 'MASSBAY':
      #NECOFS MassBay grid
      tds_url='http://www.smast.umassd.edu:8080/thredds/dodsC/FVCOM/NECOFS/Forecasts/NECOFS_FVCOM_OCEAN_MASSBAY_FORECAST.nc'
    
    
    var_name = 'zeta'
    # if waves
    if args.necofs_type == 'wave':
      var_name = 'hs'

    json = getNECOFS(tds_url, lat, lon, args.time_format, var_name)
    print json

if __name__ == "__main__":
  run()
    
