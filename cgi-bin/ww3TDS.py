#! /usr/bin/env python
import os
import argparse
import json
import netCDF4
import numpy as np
import collections
from utils.nc4_geo_idx import geo_idx
from utils.roundDateTime import roundTime
"""
WW3 THREDDS Web Service 
"""
def getWW3(tds_url, lat, lon, tf, var_name):
    """
    getWW3()  get the time series forecast from the WW3 model
      tds_url  -THREDDS OPeNDAP file url
      lat      -latitude.  decimal degrees
      lon      -longitude. decimal degrees
      tf       -time format iso or hc - return times as iso formatted strings or milliseconds since the epoch, suitable for HighCharts
      var_name -  netCDF variable name, e.g. hs (wave height) or zeta (water level)
    """

    try:
      nci = netCDF4.Dataset(tds_url)
    except RuntimeError:
      #print "ERROR: Could not open " + tds_url
      json_error = {
          "ERROR" : "Could not open " + tds_url
      }
      return json.dumps(json_error, indent=4)
    
    times = nci.variables['time']
    if not len(times):
      #print "ERROR: No times in " + tds_url
      json_error = {
          "ERROR" : "No times in " + tds_url
      }
      return json.dumps(json_error, indent=4)
    
    times = netCDF4.num2date(times[:],times.units)
    lats = nci.variables['lat'][:]
    lons = nci.variables['lon'][:]
    lat_idx = geo_idx(lat, lats)
    lon_idx = geo_idx(lon, lons)

    tds_var =  nci.variables[var_name]
    tds_var_attr =  tds_var.__dict__

    # round the times as strings this is needed. netcdf num2date for float julian days gives rounding errors in minutes.
    rnd_times = []
    for jd in times:
      rnd_times.append( roundTime(jd,roundTo=60*60))

    assert( len(times) == len(rnd_times) == len(tds_var) )

    min = tds_var[:, lat_idx, lon_idx].min()
    max = tds_var[:, lat_idx, lon_idx].max()

    # Note: WW3 grid variables are compound type, tds_var.dtype returns int16, probably for the first time dimension. 0 thru 48
    # this returns int16
    #print "tds_var.dtype", tds_var.dtype
    # this returns float32
    #print "tds_var[:].dtype", tds_var[:].dtype
    # so making this explicit
    ww3_dtype = tds_var[0,:].dtype

    # Convert times to iso strings. This is for start and end time metada
    iso_times = [ t.isoformat() for t in rnd_times]

    if tf == 'hc':
      # netcdf4 sse_units string
      sse_units = "seconds since 1970-01-01 00:00:00"
      # Convert to seconds, then milliseconds
      sse_jd = netCDF4.date2num(rnd_times, sse_units).astype('int64')
      msse = sse_jd * 1000
      # create a np.recarray() which allows us to combine ints (msse) and floats sst. Something like pandas.
      ra = np.recarray( len(msse), dtype=[ ('msse', 'int64'), (var_name, ww3_dtype)])
      ra['msse'] = msse
      ra[var_name] = tds_var[:, lat_idx, lon_idx]

    if tf == 'iso':
      # create a np.recarray() which allows us to combine string (iso_times) and floats sst. Something like pandas.
      ra = np.recarray( len(iso_times), dtype=[ ('iso_times', 'S20'), (var_name, ww3_dtype)])
      ra['iso_times'] = iso_times
      ra[var_name] = tds_var[:, lat_idx, lon_idx]

    nci.close()

    
    standard_name = ''
    if 'standard_name' in tds_var_attr:
      standard_name = tds_var_attr['standard_name']
    elif 'long_name' in tds_var_attr:
      standard_name = tds_var_attr['long_name']

    json_data = {
      "model_name": os.path.splitext(os.path.basename(tds_url))[0],
      "source_url": tds_url,
      "lat": lat,
      "lon": lon,
      "var_name": var_name,
      "standard_name": standard_name,
      "units": tds_var_attr['units'],
      "lat_idx": lat_idx,
      "lon_idx": lon_idx,
      "start_time": iso_times[0],
      "end_time": iso_times[-1],
# not serializable error
#      "min_value": min,
#      "max_value": max,
      "data": ra.tolist()
    }

    return json.dumps(json_data, indent=4)

################################################################
def run():
    parser = argparse.ArgumentParser()
    parser.add_argument("-lat", "--latitude", help="Latitude. Default 43.18", nargs='?', default=43.18)
    parser.add_argument("-lon", "--longitude", help="Longitude. Default -70.42", nargs='?', default=-70.42)
    parser.add_argument("-dt", "--data_type", help="WW3 wave model data type. height | period | dir  Default: height.", nargs='?', default="height")
    parser.add_argument("-d", "--domain", help="WW3 model domain region. GOM | EC | NA. Default: GOM", nargs='?', default="GOM")
    parser.add_argument("-tf", "--time_format", help="JSON time formats. iso strings or Highcharts (mmsse). iso | hc. Default: iso", nargs='?', default="iso")
    args = parser.parse_args()
    
    lat = float(args.latitude)
    lon = float(args.longitude)

    if args.time_format not in ['iso', 'hc']:
      print "Unknown time format", args.time_format
      exit()

    domains = {
      'GOM':'GulfOfMaine',
      'EC':'EastCoast',
      'NA':'NorthAtlantic'
    }

    data_types = {
      'height':'hs',
      'period':'swp',
      'dir':'dir'
    }

    if args.domain not in domains.keys():
      print "Unknown model domain", args.domain
      exit()

    if args.data_type not in data_types.keys():
      print "Unknown data type", args.data_type
      exit()
    
    tds_url = 'http://www.neracoos.org/thredds/dodsC/WW3/{0}.nc'.format( domains[args.domain])
    
    var_name = data_types[args.data_type]

    json = getWW3(tds_url, lat, lon, args.time_format, var_name)
    print json

if __name__ == "__main__":
  run()
    
