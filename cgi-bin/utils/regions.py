#! /usr/bin/env python

regions = {
  'GOM' : {
    'bbox' : '-70.975 -65.375 40.375 45.125'.split(),
    'name' : 'Gulf Of Maine'
  },
  # GeorgesBank
  # http://boundingbox.klokantech.com/dd
  'GB' : {
    'bbox' : '-69.873 -65.918 40.280 42.204'.split(),
    'name' : 'Georges Bank'
  },
  # GrandBanks, From Seth's GeoJSON regions.geojson
  'GB2' : {
    'bbox' : '-69.873 -64.292 39.775 42.456'.split(),
    'name' : 'Georges Bank'
  },
  'MAB' : {
    'bbox' : '-77.036 -70.005 35.389 41.640'.split(),
    'name' : 'MidAtlantic Bight'
  },
  # From Lifei's email 08/24/15
  # LAT    34.583 ~ 44.417
  # LON   -77.25 ~ -66.417
  'NESHELF' : {
    'bbox' : '-77.45 -66.35 34.50 44.50'.split(),
    'name' : 'North East Shelf'
  },
  # this EC was for Justin and Andrew.
  'EC' : {
    'bbox' : '-81.75 -65.375 25.000 45.125'.split(),
    'name' : 'East Coast'
  },
  # New NEC grid for LiFei
  #Latitude:  28.78 ~ 44.78
  #Longitude:  -81.40 ~ -63.33
  'NEC' : {
    'bbox' : '-81.45 -63.30 28.70 44.80'.split(),
    'name' : 'Northeast Coast'
  },
  'SS' : {
    'bbox' : '-66.775 -65.566 41.689 45.011'.split(),
    'name' : 'Scotian Shelf'
  }
}

#############################
if __name__ == "__main__":
  print "Region to subset: " + " | ".join(sorted(regions.keys()))
  for region in sorted(regions.keys()):
    print region,
    print regions[region]['name'],
    print regions[region]['bbox']

  # GeorgesBank
  # http://boundingbox.klokantech.com/dd
  #POLYGON((-69.873 42.2041, -65.918 42.2041, -65.918 40.2795, -69.873 40.2795, -69.873 42.2041))
