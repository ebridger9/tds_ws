#! /usr/bin/env python
"""
  getDayCount(month, day) get a 1 to 366 day from a month and day, defaults to leap years.
    - month integer > 0
    - day integer > 0  
    returns 1 thru 366, 1 - 365 for non-leap years

  getMonthDay(day, year) 
    - day integer 1 thru 366 defaults to leap year or pass a non-leap year

"""
import calendar
from datetime import date, timedelta

# Note: getDayCount could be replaced by this one liner
#d = date(2012, 2, 29)
#print d.timetuple().tm_yday
#yrday = (date(yr, mo, dy)).timetuple().tm_yday

def getDayCount(month, day, year = 2012):
  """
  return day of year, 1..366 even for non-leap years
  """
  cnt = 0
  if calendar.isleap(year):
    month_days = [31,29,31,30,31,30,31,31,30,31,30,31]
  else:
    if month == 2 and day == 29:
      return -1
    month_days = [31,28,31,30,31,30,31,31,30,31,30,31]
  for i in range(12):
    if i == (month - 1):
      break
    cnt += month_days[i]

  cnt += day
  return cnt
#######################################
def getMonthDay(day, year = 2012):
  """
  return month and day for day of year number 1 thru 366.
  Default is for leap years.  Which works for Climatologies.
  pass real year or 2010 for non-leap years. This was crucial for add_time_dimension.py
  Getting datetime from the file name.
  """
  if calendar.isleap(year):
    # a leap year first date
    first_day = date(2012, 1, 1)
  else:
    first_day = date(2010, 1, 1)
  td = timedelta(days=day-1)
  this_day = first_day + td
  return (this_day.month, this_day.day)

##########################################
def getNonLeapYears(syr, eyr):
  """
  returns list of non leap years between and including start year, end year, int[]
  - syr  int start year
  - eyr  int end year
  """
  nleapyr = []
  for yr in range(syr, eyr + 1):
    print yr
    if not calendar.isleap(yr):
      nleapyr.append(yr)

  return nleapyr


##########################################
if __name__ == "__main__":
  print "2002-2-28",  getDayCount(2,28, 2002) 
  print "2004-2-28",  getDayCount(2,28, 2004) 

  print "2002-2-29",  getDayCount(2,29, 2002) 
  print "2004-2-29",  getDayCount(2,29, 2004) 

  exit()
  """
  m,d = getMonthDay(182, 2002) 
  print m,d
  # leap 
  for i in range(1,367):
    m,d = getMonthDay(i) 
    print i, m, d
  # non-leap 
  for i in range(1,366):
    m,d = getMonthDay(i, 2010) 
    print i, m, d
  """
  m,d = getMonthDay(1) 
  print m,d
  m,d = getMonthDay(366) 
  print m,d
  m,d = getMonthDay(60) 
  print m,d

  print getDayCount(1,1) 
  print getDayCount(2,28) 
  print getDayCount(2,29, 2010) 
  print getDayCount(3,1) 
  print getDayCount(12,31) 
