#! /usr/bin/env python
from datetime import datetime
from datetime import timedelta

def roundTime(dt=None, roundTo=60):
  """Round a datetime object to any time laps in seconds
  dt : datetime.datetime object, default now.
  roundTo : Closest number of seconds to round to, default 1 minute.
  Author: Thierry Husson 2012 - Use it as you want but don't blame me.
  """
  if dt == None : dt = datetime.datetime.now()
  seconds = (dt - dt.min).seconds
  # // is a floor division, not a comment on following line:
  rounding = (seconds+roundTo/2) // roundTo * roundTo
#  return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)
  return dt + timedelta(0,rounding-seconds,-dt.microsecond)

def run():
  dt = datetime.utcnow()
  print dt.isoformat()

  rnd_dt =  roundTime(dt,roundTo=60*60)

  print rnd_dt.isoformat()

if __name__ == "__main__":
  run()

