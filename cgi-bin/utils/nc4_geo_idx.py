#! /usr/bin/env python
"""
  NetCDF functions to convert decimal degree latitude and longitude to NetCDF numpy indices.
  lats and lons inputs must come from an open NetCDF file or OPeNDAP url
"""
import os
import numpy as np

############################################
def near_ugrid_idx(lats, latitude, lons, longitude):
  """
    Note: FVCOM NECOFS and all unstructured grids work on the basis of a single grid id based on lat/lon. 
    Find the indices of the points in (lons, lats) closest to longitude, latitude
    This for unstructred grids, FVCOM (NECOFS) in particular
    - lats = numpy array of latitudes from an open NetCDF file
    - lons = numpy array of longitudes from an open NetCDF file
    - latitude float  -90 thru 90
    - longitude float -180 thru 180

      returns index of the closest triangular grid. -1 on error
  """
  grid_idx = -1
  # numpy square root on all 48,000 lons,lats
  dist = np.sqrt( ( lons-longitude )**2 + ( lats-latitude )**2 )
  grid_idx = dist.argmin()
  return grid_idx

############################################
def geo_indices(lats, latitude, lons, longitude):
  """
    - lats = numpy array of latitudes from an open NetCDF file
    - lons = numpy array of longitudes from an open NetCDF file
            lons must -180 thru 180
            if lons are 0 thru 360 they must be converted via.
            converted_lons = lons - ( lons.astype(np.int32) / 180) * 360
    - latitude float  -90 thru 90
    - longitude float -180 thru 180
  """
  lat_idx = -1
  lon_idx = -1
  lat_idx = geo_idx(latitude, lats)
  lon_idx = geo_idx(longitude, lons)
  return (lat_idx, lon_idx)

############################################
def geo_idx(dd, dd_array):
  """
    search for nearest decimal degree in an array of decimal degrees and return the index.
    np.argmin returns the indices of minium value along an axis.
    so subtract dd from all values in dd_array, take absolute value and find index of minimum.
  """
  # NOTE: this is so wrong. This would mean an exact match. We could check if the dd is between dd_array[0] and dd_array[-1]
  #if not dd in dd_array:
  #  return -1
  geo_idx = (np.abs(dd_array - dd)).argmin()
  return geo_idx


############################################
def find_oisst_bbox_indices(nci, min_lat, max_lat, min_lon, max_lon ):

  """
    returns list [min_lat_idx, max_lat_idx, min_lon_idx, max_lon_idx]
  """
  lons = nci.variables['lon'][:]
  lats = nci.variables['lat'][:]
  converted_lons = lons - ( lons.astype(np.int32) / 180) * 360
  min_lat_i, min_lon_i = geo_indices(lats, min_lat, converted_lons, min_lon)
  max_lat_i, max_lon_i = geo_indices(lats, max_lat, converted_lons, max_lon)
  return( [min_lat_i, max_lat_i, min_lon_i, max_lon_i] )

########################################################################################################################
def test_necofs():
  region = "GOM"
  tds_url='http://www.smast.umassd.edu:8080/thredds/dodsC/FVCOM/NECOFS/Forecasts/NECOFS_GOM3_FORECAST.nc'
  try:
    nci = netCDF4.Dataset(tds_url)
  except RuntimeError:
    print "ERROR: Could not open " + tds_url
    exit()
  lons = nci.variables['lon'][:]
  lats = nci.variables['lat'][:]
  # BuoyB, 43.18, -70.42
  lat = 43.18
  lon = -70.42
  # Scituate, 42.3, -70.5
  #lat = 42.3
  #lon = -70.5
  lats = nci.variables['lat'][:]
  lons = nci.variables['lon'][:]
  print lats[0], lats[-1]
  print lons[0], lons[-1]
  the_grid_idx = near_ugrid_idx( lats, lat, lons, lon)
  print lat, lon
  print the_grid_idx


def test_indices():
  region = "NESHELF"
  [min_lon, max_lon, min_lat, max_lat] = [float(i) for i in regions[region]['bbox'] ]
  tds_url = "http://www.ncdc.noaa.gov/thredds/dodsC/OISST-V2-AVHRR_agg_combined"
  try:
    nci = netCDF4.Dataset(tds_url)
  except RuntimeError:
    print "ERROR: Could not open " + tds_url
    exit()
  lons = nci.variables['lon'][:]
  lats = nci.variables['lat'][:]
  # test error
  test_idx = geo_idx(-156.00, lats)

  print test_idx

  # lats
  print "LATS"
  min_lat_idx = geo_idx(float(min_lat), lats)
  max_lat_idx = geo_idx(float(max_lat), lats)
  print region, min_lat, max_lat
  print min_lat_idx, lats[min_lat_idx], lats[min_lat_idx] + .25, lats[min_lat_idx] - .25
  print max_lat_idx, lats[max_lat_idx], lats[max_lat_idx] + .25, lats[max_lat_idx] - .25
  print "Closeness",  min_lat, lats[min_lat_idx] - min_lat
  print "Closeness",  max_lat, lats[max_lat_idx] - max_lat

  # converted_lons
  c_lons = lons - ( lons.astype(np.int32) / 180) * 360
  print "LONS"
  # lons
  min_lon_idx = geo_idx(float(min_lon), c_lons)
  max_lon_idx = geo_idx(float(max_lon), c_lons)
  print region, min_lon, max_lon
  print min_lon_idx, c_lons[min_lon_idx], c_lons[min_lon_idx] + .25, c_lons[min_lon_idx] - .25
  print max_lon_idx, c_lons[max_lon_idx], c_lons[max_lon_idx] + .25, c_lons[max_lon_idx] - .25
  print "Closeness",  min_lon, c_lons[min_lon_idx] - min_lon
  print "Closeness",  max_lon, c_lons[max_lon_idx] - max_lon

  print "Summary"
  print "min_lat_idx",min_lat_idx
  print "max_lat_idx",max_lat_idx
  print "min_lon_idx",min_lon_idx
  print "max_lon_idx",max_lon_idx

  iso_str = "2011-12-31T00:00:00"
  sdt = dateutil.parser.parse(iso_str)
  times = nci.variables['time']
  stime_idx = netCDF4.date2index(sdt, times)
  print sdt, stime_idx
  # HERE, for some unknown reason, calling this greatly speeds up find_dt_idx()
  # Perhaps this should be called in find_dt_idx? Gave up on this. 
  # for OISST and MURSST, etc. use netCDF4.date2index()
  #jd = netCDF4.num2date(times[:],times.units)
  #print jd[0].isoformat()
  #stime_idx = find_dt_idx(times, sdt)
  #print sdt, stime_idx

  nci.close()
  

#############################
def test_bbox():
  tds_url = "http://www.ncdc.noaa.gov/thredds/dodsC/OISST-V2-AVHRR_agg_combined"

  region = "NESHELF"
  [min_lon, max_lon, min_lat, max_lat] = [float(i) for i in regions[region]['bbox'] ]
  print region, min_lon, max_lon, min_lat, max_lat


  try:
    nci = netCDF4.Dataset(tds_url)
  except RuntimeError:
    print "ERROR: Could not open " + tds_url
    exit()

  lons = nci.variables['lon'][:]
  lats = nci.variables['lat'][:]

  bbox =  find_oisst_bbox_indices(nci, min_lat, max_lat, min_lon, max_lon)

  print region, min_lat, max_lat, min_lon, max_lon
  print bbox

  nci.close()

#############################
def test_ww3():
  tds_url = 'http://www.neracoos.org/thredds/dodsC/WW3/GulfOfMaine.nc'

  lat = 43.18
  lon = -70.42

  try:
    nci = netCDF4.Dataset(tds_url)
  except RuntimeError:
    print "ERROR: Could not open " + tds_url
    exit()

  lons = nci.variables['lon'][:]
  lats = nci.variables['lat'][:]
  lat_idx = geo_idx(lat, lats)
  lon_idx = geo_idx(lon, lons)

  print lat_idx, lon_idx

  nci.close()

#############################
if __name__ == "__main__":
  import dateutil.parser
  import netCDF4
  from regions import regions
  test_ww3()
  #test_indices()
  #test_necofs()
  #test_bbox()
