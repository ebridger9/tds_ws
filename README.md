# README #
*Renamed from NECOFS_WS 06/28/2016*

Stand alone Web Service for accessing UMASS Darmouth / SMAST NECOFS and BIO's WW3 Forecast Models.

### NECOFS WebService ###

* Stand alone Web Service for accessing THREDDS OPeNDAP
    * UMASS Darmouth / SMAST NECOFS Forecast Models
    * BIO WW3 Forecast Models
* Version 1.0
* returns json with forecast timeseries suitable for HighCharts along with Model Metadata.

### How do I get set up? ###

* This is a python 2.7 application.
* Configuration
* Python Dependencies
     * dateutil
     * numpy
     * netCDF4
     * Miniconda is the best way to install these dependencies [Miniconda](http://conda.pydata.org/docs/install/quick.html)
        *  *Conda and the needed installs will be included in the Docker container.*
        * conda install numpy
        * conda install netCDF4
        * conda install dateutil
        

* Deployment instructions
    * git clone https://ebridger9@bitbucket.org/ebridger9/tds_ws.git TDS_WS
    * cd to TDS_WS/
    * To run the Server in the background with default port: **8300**.  A tds_ws_httpd.log will be created in the launch directory. `tdsWS &`
    * To quit. fg;  Ctl-C;
* Tests. The application comes with an index.html with usage instructions and  links to tests.
* `http://localhost:8300/index.html`

* Repo owner @ebridger9